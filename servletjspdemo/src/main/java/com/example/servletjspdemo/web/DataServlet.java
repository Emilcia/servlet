package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/data")
public class DataServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		ServletContext app = getServletContext();
		response.setContentType("text/html");
		
		request.getSession().setAttribute("name", "");
		
		if(app.getAttribute("licznik")==null)
		{
			app.setAttribute("licznik", 1);
		}
		else
		{
		int licznik = (Integer) app.getAttribute("licznik");
		licznik++;
		app.setAttribute("licznik", licznik);
		}
		
		PrintWriter out = response.getWriter();
		
		String selectedInfo = "";
		for (String info : request.getParameterValues("info")) {
			selectedInfo += info + " ";
		}
		out.println("<html><body><h2>Your data</h2>" +
				"<p>Imie: " + request.getParameter("firstName") + "<br />" +
				"<p>Nazwisko: " + request.getParameter("surname") + "<br />" +
				"<p>Email: " + request.getParameter("email") + "<br />" +
				"<p>Nazwa pracodawcy: " + request.getParameter("employer") + "<br />" +
				"<p>Sk�d wiesz o konferencji: " + selectedInfo + request.getParameter("another") + "<br />" +
				"<p>Zajmujesz sie: " + request.getParameter("firstName") + "<br />" +
				"<p><a href='index.jsp'>Return</a></p>" +
				"</body></html>");
		out.close();
	}

}
