package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/form")
public class FormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		ServletContext app = getServletContext();

		response.setContentType("text/html");
		
		if (request.getSession().getAttribute("name")!=null)
		{
			response.sendRedirect("error");
		}
		if((app.getAttribute("licznik")!=null)&&((Integer)app.getAttribute("licznik")>4))
		{
			response.sendRedirect("error");
		}
		
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Simple form servlet</h2>" +
				"<form action='data'>" +
				"Imie: <br><input type='text' name='firstName' /> <br />" +
				"Nazwisko: <br><input type='text' name='surname' /> <br />" +
				"Email: <br><input type='text' name='email' /> <br />" +
				"Nazwa pracodawcy: <br><input type='text' name='employer' /> <br />" +
				"Skad dowiedziales sie o konferencji?<br><br>" +
				"<input type='checkbox' name='info' value='praca'>Ogloszenie w pracy<br />" +
				"<input type='checkbox' name='info' value='uczelnia'>Ogloszenie na uczelni<br />" +
				"<input type='checkbox' name='info' value='facebook'>Facebook<br />" +
				"<input type='checkbox' name='info' value='znajomi'>Znajomi<br />" +
				"Inne: <br><input type='text' name='another' /> <br /><br>" +
				"Czym sie zajmuje?:<br> <input type='text' name='hobby' /> <br />" +
				"<input type='submit' value=' OK ' />" +
				"</form>" +
				"</body></html>");
		out.close();
	}

}
